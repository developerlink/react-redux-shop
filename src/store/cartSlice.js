import { createSlice } from "@reduxjs/toolkit";

const initialCartState = {
  items: [],
  totalPrice: 0,
  changed: false,
};

const cartSlice = createSlice({
  name: "cart",
  initialState: initialCartState,
  reducers: {
    replaceCart(state, action){
      state.items = action.payload.items;
      state.totalPrice = action.payload.totalPrice;
    },
    addItem(state, action) {
      state.changed = true;
      state.totalPrice = state.totalPrice + action.payload.price;

      const existingCartItemIndex = state.items.findIndex(
        (item) => item.title === action.payload.title
      );
      const existingCartItem = state.items[existingCartItemIndex];

      if (existingCartItem) {
        existingCartItem.amount++;

        // Because of redux toolkit we can simply do ++ 'on' the item itself like above
        // and skip the below code. redux toolkit will make sure that we don't mutate the
        // existing cartItem in the state directly under the table.
        // const updatedItem = {
        //   ...existingCartItem,
        //   amount: existingCartItem.amount + 1,
        // };

        // let updatedItems;
        // updatedItems = [...state.items];
        // updatedItems[existingCartItemIndex] = updatedItem;
        // state.items = updatedItems;
      } else {
        state.items.push({ ...action.payload, amount: 1 });
        //updatedItems = state.items.concat(action.payload);
      }
    },
    removeItem(state, action) {
      state.changed = true;
      const existingCartItemIndex = state.items.findIndex(
        (item) => item.title === action.payload.title
      );
      const existingCartItem = state.items[existingCartItemIndex];
      state.totalPrice = state.totalPrice - action.payload.price;

      if (existingCartItem.amount === 1) {
        // 1 specifies how many to remove from that index forward.
        state.items.splice(existingCartItemIndex, 1);
      } else {
        existingCartItem.amount--;
      }
    }
  },
});

export const cartActions = cartSlice.actions;
export default cartSlice;
