import Card from '../UI/Card';
import classes from './Cart.module.css';
import CartItem from './CartItem';
import { useSelector } from 'react-redux';

const Cart = (props) => {
  const items = useSelector(state => state.cart.items);
  const total = useSelector(state => state.cart.totalPrice);

  return (
    <Card className={classes.cart}>
      <h2>Your Shopping Cart</h2>
      {items.length === 0 ? <h4>You have no items</h4> : ''}
      <ul>        
        {items.map(item => (
          <CartItem key={item.title}
          item={{ title: item.title, amount: item.amount, total: item.amount*item.price, price: item.price }}
          />
          ))}
      </ul>
      {items.length > 0 ? <h2>Total price: ${total}</h2> : ''}
    </Card>
  );
};

export default Cart;
