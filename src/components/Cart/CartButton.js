import classes from "./CartButton.module.css";
import { useDispatch, useSelector } from "react-redux";
import { uiActions } from "../../store/uiSlice";

const CartButton = (props) => {
  const dispatch = useDispatch();
  const itemNumber = useSelector((state) => state.cart.items.length);

  const onClickHandler = (event) => {
    dispatch(uiActions.toggle());
  };

  return (
    <button className={classes.button} onClick={onClickHandler}>
      <span>My Cart</span>
      <span className={classes.badge}>{itemNumber}</span>
    </button>
  );
};

export default CartButton;
